﻿using System;
using System.Threading.Tasks;
using DSharpPlus;
using PrintCare.Common;
using Quartz;

namespace HPCEBOT
{
    public class MessageJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            var jobDataMap = context.MergedJobDataMap;
            var client = (DiscordClient) jobDataMap["client"];
            ulong channelId = 498874589045325836;
            var message = TimeBasedCodeGenerator.Generate(TimeBasedCodeGenerator.ValiditiyPeriod.Day, 2);
            var channel = await client.GetChannelAsync(channelId);
            await client.SendMessageAsync(channel, message);
            Console.WriteLine("Message sent");
        }
    }
}