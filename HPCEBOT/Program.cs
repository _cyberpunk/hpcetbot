﻿//Created by Jay Stewart 2018



using System;
using System.Collections.Specialized;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using Quartz;
using Quartz.Impl;

namespace HPCEBOT
{
    internal class Program
    {
        private static DiscordClient client;

        private static CommandsNextExtension commands;

        private static void Main(string[] args)
        {
            new Program().MainAsync(args).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private async Task MainAsync(string[] args)
        {
            var factory = new StdSchedulerFactory(new NameValueCollection
            {
                {"quartz.scheduler.instanceName", "SchedulerBotScheduler"},
                {"quartz.jobStore.type", "Quartz.Simpl.RAMJobStore, Quartz"},
                {"quartz.threadPool.threadCount", "3"}
            });
            var scheduler = await factory.GetScheduler();
            await scheduler.Start();

            client = new DiscordClient(new DiscordConfiguration
            {
                Token = "NDk5OTc0ODc3MTI2ODUyNjA4.DqFxYA.0AgCQjMBFsbEJ82DpFq2hqmOsTM", //live
                //Token = "NTA5NDE0NzE1NDIwODM1ODYw.DsNdBQ.qsIow_7MDaWbBFeUY6A49xQTFJA", //dev 
                TokenType = TokenType.Bot,
                UseInternalLogHandler = true,
                LogLevel = LogLevel.Debug
            });

            client.GuildMemberAdded += Client_GuildMemberAdded;

            var messageJob = JobBuilder.Create<MessageJob>()
                .UsingJobData(new JobDataMap
                {
                    ["client"] = client
                })
                .Build();

            var messageTrigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule(s => s.WithIntervalInHours(24).OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(6, 0))
                    .InTimeZone(TimeZoneInfo.Utc))
                .ForJob(messageJob)
                .Build();

            await scheduler.ScheduleJob(messageJob, messageTrigger);

            string[] prefixes = {"!"};
            commands = client.UseCommandsNext(new CommandsNextConfiguration
            {
                EnableDms = false,
                EnableMentionPrefix = true,
                StringPrefixes = prefixes
            });

            commands.RegisterCommands<StandardCommands>();
            var instance = new StandardCommands();
            commands.CommandErrored += async e => { Console.WriteLine(e.Exception); };

            await client.ConnectAsync();

            await Task.Delay(-1);
        }

        private async Task Client_GuildMemberAdded(GuildMemberAddEventArgs e)
        {
            await e.Member.SendMessageAsync("Welcome " + e.Member.DisplayName + " to the HP Discord Channel. " +
                                            "To get help please type !showhelp for a list of commands. " +
                                            "Please feel free to invite your fellow workers using the same link sent to you to enable better sharing of information" +
                                            "and assisting the HP Indigo CSE's.");
        }
    }
}