﻿using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using PrintCare.Common;

namespace HPCEBOT
{

    public class StandardCommands : BaseCommandModule
    {
        static DiscordClient discord;

       

        [Command("showhelp")]
        public async Task helpme(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("Commands" + "\n" +
                                              "For Acronyms please type !(Acronym name) eg:- !AAA" + "\n" +
                                              "For Belt Tensions please type !tensions" + "\n" +
                                              "For Digitax Errors please type !digi-(error code) eg:- !digi-bt.th"
                                              
                                              );
        }

        [Command("code")]
        public async Task Code(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync(TimeBasedCodeGenerator.Generate(TimeBasedCodeGenerator.ValiditiyPeriod.Day, 2));
           
        }

        #region JigPartNumber

        [Command("Jigs")]
        public async Task Jig(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("Jigs" + "\n" +
                                              "CA090-00280 - ASSEMBLY FOR ITM DRUM." +
                                              "\n" +
                                              "CA390-02480 - ITM AND PIP DRUM REMOVAL" + "\n" +
                                              "" +
                                              "\n" +
                                              "" + "\n" +
                                              "" +
                                              "\n" +
                                              "" +
                                              "\n" +
                                              "" +
                                              "\n" +
                                              "\n" +
                                              "\n" +
                                              "Please message me(Jay Stewart) with missing Jigs you would like added");

        }
        
        #endregion

        #region Acronyms Standard

        [Command("AAA")]
        public async Task AAA(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("AAA", "Automatic Alert Agent"));            
        }

        [Command("AAP")]
        public async Task AAP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("AAP", "Adjustable Anamorphic Prism"));
        }

        [Command("ADC")]
        public async Task ADC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ADC", "Analog-Digital Converter"));
        }

        [Command("AM")]
        public async Task AM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("AM", "Screen with evenly spaced pixels (as opposed to FM, Frequency Modulation or Stochastic screen)"));
        }

        [Command("ATP")]
        public async Task ATP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ATP", "Acceptance Test Procedure"));
        }

        [Command("BDU")]
        public async Task BDU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BDU", "BID Drive Unit"));
        }

        [Command("BIU")]
        public async Task BIU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BIU", "Bid Interface unit"));
        }

        [Command("BLDC")]
        public async Task BLDC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BLDC", "Brushless DC motor"));
        }

        [Command("BSM")]
        public async Task BSM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BSM", "BID Servo Motor"));
        }

        [Command("BSMD")]
        public async Task BSMD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BSMD", "Bid Servo Motor Driver"));
        }

        [Command("BQ")]
        public async Task BQ(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BQ", "Background Qualifier"));
        }

        [Command("CAN")]
        public async Task CAN(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CAN", "Controller Area Network"));
        }

        [Command("CCC")]
        public async Task CCC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CCC", "Continuous Color Calibration"));
        }

        [Command("CCW")]
        public async Task CCW(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CCW", "Counter-Clockwise"));
        }

        [Command("CE")]
        public async Task CE(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CE", "Customer Engineer"));
        }

        [Command("CD")]
        public async Task CD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CD", "Charge Director (generic name for Imaging Agent)"));
        }

        [Command("CPLD")]
        public async Task CPLD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CPLD", "Complex Programmable Logic Device"));
        }

        [Command("CPR")]
        public async Task CPR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CPR", "Color Plane Registration"));
        }

        [Command("CRHVSC")]
        public async Task CRHVSC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CR HVSC", "Charge Roller High Voltage Stepper Control"));
        }

        [Command("CRU")]
        public async Task CRU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CRU", "Charge Roller Unit"));
        }

        [Command("CW")]
        public async Task CW(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CW", "Clockwise"));
        }
        
        [Command("DA")]
        public async Task DA(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DA", "Dot Area"));
        }

        [Command("DCB")]
        public async Task DCB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DCB", "Dimmer Control Board"));
        }

        [Command("DCD")]
        public async Task DCD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DCD", "Direct Current Distribution"));
        }

        [Command("DCI")]
        public async Task DCI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DCI", "Drum Control Interface"));
        }

        [Command("DFC")]
        public async Task DFC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DFC", "Differential bow compensation by Format Correction"));
        }

        [Command("DFE")]
        public async Task DFE(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DFE", "Digital Front End"));
        }

        [Command("DFS")]
        public async Task DFS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DFS", "Dynamic Flow System"));
        }

        [Command("DFR")]
        public async Task DFR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DFR", "Duplex Feed Roller"));
        }

        [Command("DMC")]
        public async Task DMC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DMC", "Dynamic Mirror Controller"));
        }

        [Command("DPI")]
        public async Task DPI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DPI", "Dots Per Inch"));
        }

        [Command("DSC")]
        public async Task DSC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DSC", "DSP Servo Controller (DSP = Digital Signal Processor)"));
        }

        [Command("DVM")]
        public async Task DVM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DVM", "Digital Volt Meter"));
        }

        [Command("EBM")]
        public async Task EBM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EBM", "Easy Blanket Mounting"));
        }

        [Command("EBV")]
        public async Task EBV(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EBV", "Exit Blanket Ventilation"));
        }

        [Command("EC")]
        public async Task EC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EC", "Exit Conveyor"));
        }

        [Command("E2E")]
        public async Task E2E(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("E2E", "End to End"));
        }

        [Command("EFIGS")]
        public async Task EFIGS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EFIGS", "English, French, Italian, Spanish"));
        }

        [Command("EHH")]
        public async Task EHH(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EH(H)", "External Heater (Housing)"));
        }

        [Command("EI")]
        public async Task EI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EI", "ElektroInk"));
        }

        [Command("ELF")]
        public async Task ELF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ELF", "Electrical Fatigue"));
        }

        [Command("EMC")]
        public async Task EMC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EMC", "Electro Magnetic Charge"));
        }

        [Command("EPM")]
        public async Task EPM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EPM", "Enhanced Productivity Mode"));
        }

        [Command("ESD")]
        public async Task ESD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ESD", "ESD"));
        }
        
        [Command("FDT")]
        public async Task FDT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FDT", "Feeder Diagnostic Tool"));
        }

        [Command("FI")]
        public async Task FI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FI ", "Fluid Interface or Fluid Injector (BID connector)"));
        }

        [Command("FLC")]
        public async Task FLC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FLC", "Finishing Line Controller"));
        }

        [Command("FFT")]
        public async Task FFT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FFT", "Fast Fourier Transform"));
        }

        [Command("FM")]
        public async Task FM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FM", "Stochastic (Frequency Modulation) screening"));
        }

        [Command("FOS")]
        public async Task FOS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FOS", "Forked Ozone Suction"));
        }
        

        [Command("FP")]
        public async Task FP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FP", "Focal Plane"));
        }

        [Command("FPGA")]
        public async Task FPGA(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FPGA", "Field Programmable Gate Array"));
        }

        [Command("FRU")]
        public async Task FRU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FRU", "Field Replaceble Unit"));
        }

        [Command("FTB")]
        public async Task FTB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FTB", "Front To Back"));
        }

        [Command("FTF")]
        public async Task FTF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FTF", "Front To Front"));
        }

        [Command("GBU")]
        public async Task GBU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("GBU", "Global Business Unit"));
        }

        [Command("GDU")]
        public async Task GDU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("GDU", "Gripper Drive Unit"));
        }

        [Command("GLINK")]
        public async Task GLINK(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("GLINK", "Graphics Link"));
        }

        [Command("GUI")]
        public async Task GUI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("GUI", "Graphic User Interface"));
        }

        [Command("GLS")]
        public async Task GLS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("GLS", "Gear Lubrication System"));
        }

        [Command("GND")]
        public async Task GND(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("GND", "GrouND"));
        }

        [Command("GSM")]
        public async Task GSM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("GSM", "Gram per Square Meter"));
        }

        [Command("HCB")]
        public async Task HCB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HCB", "Heater Control Board"));
        }

        [Command("HCPR")]
        public async Task HCPR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HCPR", "Horizontal Color Plane Registration"));
        }

        [Command("HDI")]
        public async Task HDI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HDI", "High Definition Imaging"));
        }

        [Command("HE")]
        public async Task HE(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HE", "Heat Exchanger"));
        }

        [Command("HTS")]
        public async Task HTS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HTS", "Harmonic Topology Screening"));
        }

        [Command("HVSC")]
        public async Task HVSC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HVSC", "High Voltage Stepper Control"));
        }

        [Command("HVIF")]
        public async Task HVIF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HVIF", "High Voltage Interface"));
        }

        [Command("HVPS")]
        public async Task HVPS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HVPS", "High Voltage Power Supply"));
        }

        [Command("HX")]
        public async Task HX(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HX", "Heat Exchanger"));
        }

        [Command("IA")]
        public async Task IA(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("IA", "Imaging Agent"));
        }

        [Command("ICD")]
        public async Task ICD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ICD", "Interface Control Document"));
        }

        [Command("ICL")]
        public async Task ICL(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ICL", "Imaging Oil Conductivity and Level"));
        }

        [Command("ICS")]
        public async Task ICS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ICS", "Interoperability Conformance Specifications"));
        }

        [Command("ILF")]
        public async Task ILF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ILF", "InLine Finisher"));
        }

        [Command("IFR")]
        public async Task IFR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("IFR", "Input Feed Roller"));
        }

        [Command("I/O")]
        public async Task IO(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("I/O", "Input/Output"));
        }

        [Command("ILS")]
        public async Task ILS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ILS", "InLine Scanner"));
        }

        [Command("ILD")]
        public async Task ILD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ILD", "InLine Densitometer"));
        }

        [Command("IMP")]
        public async Task IMP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("IMP", "Impression (drum)"));
        }

        [Command("IOC")]
        public async Task IOC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("IOC", "Imaging Oil Control"));
        }

        [Command("ITM")]
        public async Task ITM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ITM", "Intermediate Transfer Media"));
        }

        [Command("ITT")]
        public async Task ITT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ITT", "Image Tracking Technology"));
        }
        
        [Command("JDF")]
        public async Task JDF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("JDF", "Job Definition Format"));
        }

        [Command("JMF")]
        public async Task JMF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("JMF", "Job Messaging Format"));
        }

        [Command("LDC")]
        public async Task LDC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("LDC", "Laser Diode Controller"));
        }

        [Command("LDD")]
        public async Task LDD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("LDD", "Laser Diode Driver"));
        }

        [Command("LED")]
        public async Task LED(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("LED", "Light Emitting Diode"));
        }

        [Command("LE")]
        public async Task LE(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("LE", "Leading Edge"));
        }

        [Command("LPI")]
        public async Task LPI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("LPI", "Lines Per Inch"));
        }

        [Command("LUT")]
        public async Task LUT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("LUT", "Look-Up Table"));
        }

        [Command("MCN")]
        public async Task MCN(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MCN", "Main Control Node"));
        }

        [Command("MSC")]
        public async Task MSC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MSC", "Machine Speed Control"));
        }

        [Command("MIC")]
        public async Task MIC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MIC", "Master Ink Controller"));
        }

        [Command("MLA")]
        public async Task MLA(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MLA", "Monolithic Laser Array"));
        }

        [Command("MMI")]
        public async Task MMI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MMI", "Man Machine Interface"));
        }

        [Command("MNF")]
        public async Task MNF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MNF", "Molex Minifit (connector)"));
        }

        [Command("MOP")]
        public async Task MOP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MOP", "Motor On Press"));
        }

        [Command("MRHC")]
        public async Task MRHC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MRHC", "Machine Relative Humidity Control"));
        }

        [Command("MTBF")]
        public async Task MTBF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MTBF", "Mean Time Before Fix"));
        }

        [Command("MTC")]
        public async Task MTC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MTC", "Machine Temperature Control"));
        }

        [Command("MTCRH")]
        public async Task MTCRH(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MTCRH", "Machine Temperature Control Relative Humidity"));
        }
        
        [Command("NLF")]
        public async Task NLF(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("NLF", "Near Line Finisher"));
        }

        [Command("NVS")]
        public async Task NVS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("NVS", "Non-Volatile Solid"));
        }

        [Command("OCB")]
        public async Task OCB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("OCB", "Optics Connector Board"));
        }

        [Command("OD")]
        public async Task OD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("OD", "Optical Density"));
        }

        [Command("OFIR")]
        public async Task OFIR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("OFIR", "On Press Fast Ink Replacement"));
        }

        [Command("OMP")]
        public async Task OMP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("OMP", "Operator Maintenance Part"));
        }

        [Command("OPD")]
        public async Task OPD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("OPD", "On Press Documentation"));
        }

        [Command("OPS")]
        public async Task OPS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("OPS", "Old PIP Syndrome"));
        }

        [Command("ORS")]
        public async Task ORS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ORS", "Oil Reuse System"));
        }

        [Command("OW")]
        public async Task OW(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("OW", "Operating Window"));
        }
        
        [Command("PDB")]
        public async Task PDB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PDB", "Power Distribution Board"));
        }

        [Command("PCB")]
        public async Task PCB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PCB", "Printed Circuit Board"));
        }

        [Command("PCN")]
        public async Task PCN(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PCN", "Paper Control Node"));
        }

        [Command("PDU")]
        public async Task PDU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PDU ", "Power Distribution Unit"));
        }

        [Command("PE")]
        public async Task PE(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PE", "Printing Engine"));
        }

        [Command("PEU")]
        public async Task PEU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PEU", "PIP Enhancement Unit"));
        }

        [Command("PHC")]
        public async Task PHC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PHC", "Paper Handling Controller"));
        }

        [Command("PID")]
        public async Task PID(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PID", "Partial Ink Development"));
        }

        [Command("PIP")]
        public async Task PIP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PIP", "Printing Imaging Plate"));
        }

        [Command("PLD")]
        public async Task PLD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PLD", "Polygon Drive"));
        }

        [Command("PPM")]
        public async Task PPM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PPM", "Pages Per Minute"));
        }
        

        [Command("PQ")]
        public async Task PQ(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PQ", "Print Quality"));
        }

        [Command("PS")]
        public async Task PS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PS", "Paper Sensor"));
        }

        [Command("PSTB")]
        public async Task PSTB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PSTB", "Paper Stuck to Blanket"));
        }

        [Command("PSU")]
        public async Task PSU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PSU", "Printing Stopped Unexpectedly"));
        }

        [Command("PTE")]
        public async Task PTE(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PTE", "Pre-Transfer Erase (lamp)"));
        }

        [Command("PTP")]
        public async Task PTP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PTP", "Peak To Peak"));
        }

        [Command("PWM")]
        public async Task PWM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PWM", "Pulse Width Modulation"));
        }
        
        [Command("RA")]
        public async Task RA(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RA ", "Recycling Agent"));
        }

        [Command("RET")]
        public async Task RET(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RET", "Resolution Enhancement Technology"));
        }

        [Command("RMS")]
        public async Task RMS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RMS", "Root Mean Square"));
        }

        [Command("RoHS")]
        public async Task RoHS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RoHS", "Restricted of Hazards Substances"));
        }

        [Command("ROI")]
        public async Task ROI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ROI", "Return On Investment"));
        }

        [Command("ROR")]
        public async Task ROR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ROR", "Reason Of Replacement"));
        }

        [Command("RPM")]
        public async Task RPM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RPM", "Remote Production Manager"));
        }

        [Command("USB")]
        public async Task USB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("USB", "Universal Serial Bus"));
        }

        [Command("UPS")]
        public async Task UPS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("UPS", "Uninterruptable Power Supply"));
        }

        [Command("UFIC")]
        public async Task UFIC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("UFIC", "Universal Finisher Interface Controller"));
        }

        [Command("UFI")]
        public async Task UFI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("UFI", "Universal Finisher Interface"));
        }

        [Command("RU")]
        public async Task RU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RU", "Relay UPS"));
        }

        [Command("SC")]
        public async Task SC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SC", "Suction Cup "));
        }

        [Command("SCM")]
        public async Task SCM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SCM", "Suction Cup Margin"));
        }

        [Command("SG")]
        public async Task SG(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SG", "Silica Gel"));
        }

        [Command("SOS")]
        public async Task SOS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SOS ", "Start Of Scan"));
        }

        [Command("SOSC")]
        public async Task SOSC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SOSC", "Start Of Scan Camera"));
        }

        [Command("SPV")]
        public async Task SPV(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SPV", "Statistical Process Viewer"));
        }

        [Command("SR")]
        public async Task SR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SR", "Safety Relay"));
        }

        [Command("SSB")]
        public async Task SSB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SSB", "Stepper Switch Board"));
        }

        [Command("SSC")]
        public async Task SSC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SSC", "Stepper System Control"));
        }

        [Command("SSIM")]
        public async Task SSIM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SSIM", "Structural Similarity"));
        }

        [Command("SSR")]
        public async Task SSR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SSR", "Solid State Relay"));
        }

        [Command("STD")]
        public async Task STD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("STD", "Stepper Drive"));
        }

        [Command("T0")]
        public async Task T0(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("T0", "Ink Transfer from BID to PIP"));
        }

        [Command("T1")]
        public async Task T1(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("T1", "Ink Transfer from PIP to Blanket"));
        }

        [Command("T2")]
        public async Task T2(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("T2", "Ink Transfer from Blanket to Substrate"));
        }

        [Command("TBD")]
        public async Task TBD(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("TBD", "To Be Determined"));
        }

        [Command("TCE")]
        public async Task TCE(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("TCE", "Total Customer Experience"));
        }

        [Command("TCO")]
        public async Task TCO(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("TCO", "Total Cost of Ownership"));
        }

        [Command("TE")]
        public async Task TE(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("TE", "Trailing Edge"));
        }

        [Command("TIC")]
        public async Task TIC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("TIC", "Tank Ink Controller"));
        }

        [Command("TIRS")]
        public async Task TIRS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("TIRS", "Temperature Infra Red (system) Sensor"));
        }

        [Command("TMC")]
        public async Task TMC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("TMC", "Temperature Control"));
        }

        [Command("TTR")]
        public async Task TTR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("TTR", "Time To Repair"));
        }

        [Command("UART")]
        public async Task UART(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("UART", "Universal Asynchronous Receiver/Transmitter"));
        }

        [Command("UCN")]
        public async Task UCN(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("UCN", "Utillity Cabinet Node"));
        }

        [Command("UTK")]
        public async Task UTK(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("UTK", "Up Time Kit"));
        }

        [Command("UI")]
        public async Task UI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("UI", "User Interface"));
        }

        [Command("VCC")]
        public async Task VCC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("VCC", "Voltage at the Common Collector"));
        }

        [Command("Vcorn")]
        public async Task Vcorn(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("Vcorn", "From Video and Unicorn"));
        }

        [Command("VCPR")]
        public async Task VCPR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("VCPR", "Vertical Color Plane Registration"));
        }

        [Command("VOC")]
        public async Task VOC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("VOC", "Utillity Cabinet Node"));
        }

        [Command("WHEL")]
        public async Task WHEL(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("WHEL", "Writing Head Electronics 24"));
        }

        #endregion

        #region Acronyms 10000

        [Command("BARS")]
        public async Task BARS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BARS", "Blanket Auto Replace System"));
        }

        [Command("BDT")]
        public async Task BDT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BDT", "Manufacturer of the S4 Feeder and Stacker"));
        }

        [Command("BES")]
        public async Task BES(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BES", "Bid Engage System"));
        }

        [Command("BHV")]
        public async Task BHV(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BHV", "BID High Voltage"));
        }

        [Command("BIM")]
        public async Task BIM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BIM", "BID Ink Manager"));
        }

        [Command("CRI")]
        public async Task CRI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CRI", "Charge Roller Interface"));
        }

        [Command("CVC")]
        public async Task CVC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CVC", "Climate and Volatile Organic Compounds Control"));
        }

        [Command("CTIC")]
        public async Task CTIC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CTIC", "ITEC for tank ink common functions"));
        }

        [Command("CWS")]
        public async Task CWS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CWS", "Cold Water System"));
        }

        [Command("DLC")]
        public async Task DLC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DLC", "Duplex Linear Conveyor"));
        }

        [Command("DM")]
        public async Task DM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DM", "Dynamic Mirror"));
        }

        [Command("DPC")]
        public async Task DPC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DPC", "Data Process Control"));
        }

        [Command("DT")]
        public async Task DT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DT", "Data Process Control"));
        }

        [Command("DVC")]
        public async Task DVC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DVC", "Duplex Vacuum Conveyor"));
        }

        [Command("ECM")]
        public async Task ECM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ECM", "Engine Control Manager"));
        }

        [Command("ECP")]
        public async Task ECP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ECP", "Engine Control Power"));
        }

        [Command("EFM")]
        public async Task EFM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EFM", "External Folding Mirror"));
        }

        [Command("EPC")]
        public async Task EPC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EPC", "Exit Pickup Conveyor"));
        }

        [Command("EPL")]
        public async Task EPL(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EPL", "Ethernet PowerLink"));
        }

        [Command("ESC")]
        public async Task ESC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ESC", "Exit Scanner Conveyor"));
        }

        [Command("EVR")]
        public async Task EVR(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("EVR", "Exit Vacuum Rotar"));
        }

        [Command("FMC")]
        public async Task FMC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("FMC", "Feed Management Controller"));
        }

        [Command("IDU")]
        public async Task IDU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("IDU", "Ink Drive Unit (mixer) Also Ink Dispersion Unit"));
        }

        [Command("IFM")]
        public async Task IFM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("IFM", "Ink Flow Meter"));
        }

        [Command("IPC")]
        public async Task IPC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("IPC", "Indigo Press Controller"));
        }

        [Command("ITC")]
        public async Task ITC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ITC", "Impression Temperature Control"));
        }

        [Command("ITEC")]
        public async Task ITEC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ITEC", "Ink Tank Electronic Control"));
        }

        [Command("ITS")]
        public async Task ITS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ITS", "Ink Tank Sensors"));
        }

        [Command("JOS")]
        public async Task JOS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("JOS", "Jump Over Seam"));
        }

        [Command("KVM")]
        public async Task KVM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("KVM", "Keyboard, Video, Mouse"));
        }

        [Command("LCS")]
        public async Task LCS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("LCS", "Level Continuous Sensor"));
        }

        [Command("NDG")]
        public async Task NDG(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("NDG", "Negative Dot Gain"));
        }

        [Command("ODS")]
        public async Task ODS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ODS", "Optical Density Sensor"));
        }

        [Command("PARS")]
        public async Task PARS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PARS", "PIP Auto Replace System"));
        }

        [Command("PCM")]
        public async Task PCM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PCM", "Paper Handling Control Manager"));
        }

        [Command("PBS")]
        public async Task PBS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PBS", "Polarized Beam Splitter"));
        }

        [Command("PLC")]
        public async Task PLC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PLC", "Programmable Logic Control"));
        }

        [Command("RFID")]
        public async Task RFID(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RFID", "Radio Frequency ID"));
        }

        [Command("RJ")]
        public async Task RJ(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RJ", "Rotary Joint"));
        }

        [Command("SAB")]
        public async Task SAB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SAB", "Amplifier 5 to 24V"));
        }

        [Command("SAS")]
        public async Task SAS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SAS", "Solid Add System"));
        }

        [Command("SLC")]
        public async Task SLC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SLC", "Safety Logic Control"));
        }

        [Command("SMS")]
        public async Task SMS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SMS", "Safe Maximum Speed"));
        }

        [Command("SSL")]
        public async Task SSL(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SSL", "Safety Speed Limit"));
        }

        [Command("SPM")]
        public async Task SPM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SPM", "Spectrophotometer"));
        }

        [Command("SS1")]
        public async Task SS1(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SS1", "Safe Stop 1"));
        }

        [Command("STO")]
        public async Task STO(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("STO", "Safe Torque Off"));
        }

        [Command("VSP")]
        public async Task VSP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("VSP", "Verticle Substrate Path"));
        }

        [Command("CBB")]
        public async Task CBB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CBB", "Control Board Type-B"));
        }






        #endregion

        #region Acroynms 5000

        [Command("HAL")]
        public async Task HAL(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HAL", "Hardware Abstraction Layer (appears in initialization window)"));

        }

        [Command("SCU")]
        public async Task SCU(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SCU", "Scorotron Charge Unit (SC HV, PIP motors (SCU1) and home sensor)"));

        }

        [Command("CMB")]
        public async Task CMB(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CMB", "Current Monitoring Board (in PDU of original S2 configuration)"));

        }

        [Command("PCA")]
        public async Task PCA(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PCA", "Printed Circuit Assembly board"));

        }

        [Command("MP")]
        public async Task MP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("MP", "Diverter Multi-pick Tray"));

        }

        [Command("VPP")]
        public async Task VPP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("VPP", "Verticle Paper Path"));

        }

        [Command("PTS")]
        public async Task PTS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PTS", "Paper Thickness sensor "));

        }

        [Command("PPS")]
        public async Task PPS(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PPS", "Paper Presence Sensor"));

        }

        [Command("RIP")]
        public async Task RIP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RIP", "Paper Presence SensorRaster Imaging Processor"));

        }

        [Command("DHCP")]
        public async Task DHCP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DHCP", "Dynamic Host Configuration Protocol "));

        }

        [Command("DFM")]
        public async Task DFM(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("DFM", "Design for manufacturing"));

        }

        [Command("HALT")]
        public async Task HALT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("HALT", "Highly Accelerated Life Test "));

        }

        [Command("ICC")]
        public async Task ICC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("ICC", "International Color Consortium"));

        }

        [Command("CSA")]
        public async Task CSA(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("CSA", "Color Space Array"));

        }

        [Command("RAID")]
        public async Task RAID(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RAID", "Redundant Array of Inexpensive Disks"));

        }

        [Command("RIT")]
        public async Task RIT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("RIT", "Relay ITM Thermostat"));

        }

        [Command("PPML")]
        public async Task PPML(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("PPML", "Personalised Printing Markup Language"));

        }

        [Command("OPI")]
        public async Task OPI(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("OPI", "Open Pre-press Interface (low res images replace high res images before ripping; high res images sit on a server waiting for the job to be sent)"));

        }

        [Command("LIC")]
        public async Task LIC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("LIC", "large Ink Can"));

        }

        #endregion

        #region 7X00 Acroynms

        [Command("BIP")]
        public async Task BIP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("BIP", "Bid In Place"));

        }

        [Command("SR1")]
        public async Task SR1(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SR1", "Safety Relay 1. SR1 regulates the power to the external heaters. Press must be above 80% of full speed before the external lamps can be energized."));

        }

        [Command("SR2")]
        public async Task SR2(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.AddField("SR2", "Safety Relay 2. SR2 regulates the control of K2 - the interlock relay, and the Main Motor relay.Located in the PDU."));

        }

        


        #endregion

        #region DigitaxST

        [Command("Digi-br.th")]
        public async Task Brth(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = br.th").AddField("Braking resistor thermistor temperature monitoring fail", "If no brake resistor is installed, set Pr 0.51 (or Pr 10.37) to 8 to disable this trip.If a brake resistor is installed:Ensure that the braking resistor thermistor is connected correctly Ensure that the fan in the drive is working correctly Replace the braking resistor"));
        }

        [Command("Digi-C.Acc")]
        public async Task CACC(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.Acc").AddField("SMARTCARD trip: SMARTCARD Read / Write fail", "Check SMARTCARD is installed / located correctly Ensure SMARTCARD is not writing data to data location 500 to 999 Replace SMARTCARD"));
        }

        [Command("Digi-C.boot")]
        public async Task CBOOT(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.boot").AddField("SMARTCARD trip: The menu 0 parameter modification cannot be saved to the SMARTCARD because the necessary file has not been created on the SMARTCARD", "A write to a menu 0 parameter has been initiated via the keypad with Pr 11.42 set to auto(3) or boot(4), but the necessary file on the SMARTCARD has not bee created Ensure that Pr 11.42 is correctly set and reset the drive to create the necessary file on the SMARTCARD Re - attempt the parameter write to the menu 0 parameter"));
        }

        [Command("Digi-C.bUSY")]
        public async Task CbUSY(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.bUSY").AddField("SMARTCARD trip: The menu 0 parameter modification cannot be saved to the SMARTCARD because the necessary file has not been created on the SMARTCARD", "A write to a menu 0 parameter has been initiated via the keypad with Pr 11.42 set to auto(3) or boot(4), but the necessary file on the SMARTCARD has not been created. Ensure that Pr 11.42 is correctly set and reset the drive to create the necessary file on the SMARTCARD Re-attempt the parameter write to the menu 0 parameter"));
        }

        [Command("Digi-C.Chg")]
        public async Task CChg(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.Chg").AddField("SMARTCARD trip: Data location already contains data", "Erase data in data location and Write data to an alternative data location"));
        }

        [Command("Digi-C.dAt")]
        public async Task CdAt(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.dAt").AddField("SMARTCARD trip: Data location specified does not contain any data", "Ensure data block number is correct"));
        }

        [Command("Digi-C.Err")]
        public async Task CErr(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.Err").AddField("SMARTCARD trip: SMARTCARD data is corrupted", "Ensure the card is located correctly, Erase data and retry, Replace SMARTCARD"));
        }

        [Command("Digi-C.Full")]
        public async Task CFull(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.Full").AddField("SMARTCARD trip: SMARTCARD full", "Delete a data block or use different SMARTCARD"));
        }

        [Command("Digi-cL2")]
        public async Task cL2l(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = cL2").AddField("Analog input 2 current loss (current mode)", "Check analog input 2 (terminal 7) current signal is present (4-20mA, 20-4mA)"));
        }

        [Command("Digi-cL3")]
        public async Task cL3(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = cL3").AddField("Analog input 3 current loss (current mode)", "Check analog input 3 (terminal 8) current signal is present (4-20mA, 20-4mA)"));
        }

        [Command("Digi-CL.bit")]
        public async Task CLbit(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = CL.bit").AddField("Trip initiated from the control word (Pr 6.42)", "Disable the control word by setting Pr 6.43 to 0 or check setting of Pr 6.42"));
        }

        [Command("Digi-C.OPtn")]
        public async Task COPtn(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.OPtn").AddField("SMARTCARD trip: Solutions Modules installed are different between source drive and destination drive", "Ensure correct Solutions Modules are installed, Ensure Solutions Modules are in the same Solutions Module slot, Press the red reset button"));
        }

        [Command("Digi-C.Prod")]
        public async Task CProd(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.Prod").AddField("SMARTCARD trip: The data blocks on the SMARTCARD are not compatible with this product", "Erase all data on the SMARTCARD by setting Pr xx.00 to 9999 and pressing the red reset button, Replace SMARTCARD"));
        }

        [Command("Digi-C.rdo")]
        public async Task Crdo(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.rdo").AddField("SMARTCARD trip: SMARTCARD has the Read Only bit set", "Enter 9777 in Pr xx.00 to allow SMARTCARD Read / Write access, Ensure card is not writing to data locations 500 to 999"));
        }

        [Command("Digi-C.TyP")]
        public async Task CTyP(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("", embed: new DiscordEmbedBuilder()
            {
                Color = new DiscordColor("89ff89")
            }.WithDescription("Error = C.TyP").AddField("SMARTCARD trip: SMARTCARD parameter set not compatible with drive", "Press the reset button, Ensure destination drive type is the same as the source parameter file drive type"));
        }

        [Command("Tensions")]
        public async Task Tension(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync("Belt Tensions" + "\n" +
                                              "FEED ASSY - New Belt(60.3hz - 63.3hz) ~ Used Belt(50.5hz - 53.9hz)." +
                                              "\n" +
                                              "REINSERTION ASSY - 130hz" + "\n" +
                                              "FI MAIN ASSY - New Belt(167hz - 175hz) ~ Used Belt(140hz - 150hz)" +
                                              "\n" +
                                              "IBS MAIN ASSY - 20.2hz - 22.3hz" + "\n" +
                                              "FE MAIN ASSY - New Belt(75hz - 75.8hz) ~ Used Belt(62.8hz - 67.1hz)" +
                                              "\n" +
                                              "DRIVE UNIT ASSY - New Belt(40hz - 42hhz) ~ Used Belt(34.2hz - 36.5hz)" +
                                              "\n" +
                                              "ILP FU - New Belt(167hz - 175hz) ~ Used Belt(140hz - 150hz)" +
                                              "\n"+
                                              "\n" +
                                              "\n" +
                                              "Please message me(Jay Stewart) with belt tensions you would like added");

        }

    }
    #endregion

        #region JigNumbers

    

    #endregion




}

